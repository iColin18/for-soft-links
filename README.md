# 使用思岚A2激光雷达结合cartographer建图算法手持建图

## **一、将思岚的雷达驱动包放在src目录下并编译测试**![输入图片说明](https://img-blog.csdnimg.cn/20200803193332689.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4MjEyNzg3,size_16,color_FFFFFF,t_70 "在这里输入图片标题")

![img](https://img-blog.csdnimg.cn/20200803193332689.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4MjEyNzg3,size_16,color_FFFFFF,t_70)

> ```bash
> catkin_make_isolated --install --use-ninja
> 
> roslaunch rplidar_ros rplidar.launch
> ```

雷达转起来后，命令窗口如下所示：

![img](https://img-blog.csdnimg.cn/20200803193542442.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4MjEyNzg3,size_16,color_FFFFFF,t_70)

## **二、修改revo_lds.lua文件**

文件路径：

```
~/catkin_google_ws/src/cartographer_ros/cartographer_ros/configuration_files/revo_lds.lua
```

修改之前文件内容如下：

```c++
include "map_builder.lua"
include "trajectory_builder.lua"
options =
{
map_builder = MAP_BUILDER,
trajectory_builder = TRAJECTORY_BUILDER,
map_frame = "map",
tracking_frame = "laser",
published_frame = "laser",
odom_frame = "odom",
provide_odom_frame = true,
publish_frame_projected_to_2d = false,
use_pose_extrapolator = on,
use_odometry = false,
use_nav_sat = false,
use_landmarks = false,
num_laser_scans = 1,
num_multi_echo_laser_scans = 0,
num_subdivisions_per_laser_scan = 1,
num_point_clouds = 0,
lookup_transform_timeout_sec = 0.2,
submap_publish_period_sec = 0.3,
pose_publish_period_sec = 5e-3,
trajectory_publish_period_sec = 30e-3,
rangefinder_sampling_ratio = 1.,
odometry_sampling_ratio = 1.,
fixed_frame_pose_sampling_ratio = 1.,
imu_sampling_ratio = 1.,
landmarks_sampling_ratio = 1.,
}
MAP_BUILDER.use_trajectory_builder_2d = true
TRAJECTORY_BUILDER_2D.submaps.num_range_data = 35
TRAJECTORY_BUILDER_2D.min_range = 0.3
TRAJECTORY_BUILDER_2D.max_range = 8.
TRAJECTORY_BUILDER_2D.missing_data_ray_length = 1.
TRAJECTORY_BUILDER_2D.use_imu_data = false
TRAJECTORY_BUILDER_2D.use_online_correlative_scan_matching = true
TRAJECTORY_BUILDER_2D.real_time_correlative_scan_matcher.linear_search_window = 0.1
TRAJECTORY_BUILDER_2D.real_time_correlative_scan_matcher.translation_delta_cost_weight = 10.
TRAJECTORY_BUILDER_2D.real_time_correlative_scan_matcher.rotation_delta_cost_weight = 1e-1
TRAJECTORY_BUILDER.collate_landmarks = on
POSE_GRAPH.optimization_problem.huber_scale = 1e2
POSE_GRAPH.optimize_every_n_nodes = 35
POSE_GRAPH.constraint_builder.min_score = 0.65

return options
```

> **注意：`use_pose_extrapolator = on`, 这个之前官方写的是true,按照我参考的博文测试，报错：**

```bash
F0803 19:00:47.287873 14354 lua_parameter_dictionary.cc:410] Check failed: 1 == reference_counts_.count(key) (1 vs. 0) Key 'use_pose_extrapolator' was used the wrong number of times. [FATAL] [1596452447.288220107]: F0803 19:00:47.000000 14354 lua_parameter_dictionary.cc:410] Check failed: 1 == reference_counts_.count(key) (1 vs. 0) Key 'use_pose_extrapolator' was used the wrong number of times. *** Check failure stack trace: *** @ 0x7fd6681080cd google::LogMessage::Fail() @ 0x7fd668109f33 google::LogMessage::SendToLog() @ 0x7fd668107c28 google::LogMessage::Flush() @ 0x7fd66810a999 google::LogMessageFatal::~LogMessageFatal() @ 0x562312a188a6 (unknown) @ 0x562312a18a75 (unknown) @ 0x5623129ff0a3 (unknown) @ 0x5623129d4a33 (unknown) @ 0x5623129d25b4 (unknown) @ 0x7fd663e31b97 __libc_start_main @ 0x5623129d472a (unknown) [cartographer_node-2] process has died [pid 14354, exit code -6, cmd /home/ubuntu/catkin_google_ws/install_isolated/lib/cartographer_ros/cartographer_node -configuration_directory /home/ubuntu/catkin_google_ws/install_isolated/share/cartographer_ros/configuration_files -configuration_basename revo_lds.lua scan:=scan __name:=cartographer_node __log:=/home/ubuntu/.ros/log/9549cd64-d578-11ea-98be-28392648c2f5/cartographer_node-2.log]. log file: /home/ubuntu/.ros/log/9549cd64-d578-11ea-98be-28392648c2f5/cartographer_node-2*.log
```

解决方案：

参考国外的博文，故加上on,总共加了两句：

***

```bash
use_pose_extrapolator = on,

TRAJECTORY_BUILDER.collate_landmarks = on
```

里面的`horizontal_laser_link`，改成自己激光雷达的`frame_id`，我的是`laser`，一般都是`laser`，其余的不要动。

## **三、修改demo_revo_lds.launch文件**

定位到文件路径：

```
~/catkin_google_ws/src/cartographer_ros/cartographer_ros/launch/demo_revo_lds.launch
```

`demo_revo_lds.launch`并修改代码如下：

```xml
<!--
  Copyright 2016 The Cartographer Authors
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<launch>
  <param name="/use_sim_time" value="true" />
  <node name="cartographer_node" pkg="cartographer_ros"
      type="cartographer_node" args="
          -configuration_directory $(find cartographer_ros)/configuration_files
          -configuration_basename revo_lds.lua"
      output="screen">
    <remap from="scan" to="scan" />
  </node>
  <node name="rviz" pkg="rviz" type="rviz" required="true"
      args="-d $(find cartographer_ros)/configuration_files/demo_2d.rviz" />" 
</launch>
```

修改完之后，回到`cartographer_ws` 目录下再次编译：

```
catkin_make_isolated --install --use-ninja
```

## **四、 启动测试**

（1）首先运行激光雷达节点`launch`文件

```
roslaunch rplidar_ros rplidar.launch
```

（2）运行`cartographer`框架

```
roslaunch cartographer_ros demo_revo_lds.launch
```

运行正常将打开rviz，并且显示正常，如下：

![img](https://img-blog.csdnimg.cn/20200803195138690.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM4MjEyNzg3,size_16,color_FFFFFF,t_70)

完成雷达的测试。



## 参考：

1. https://blog.csdn.net/u014662384/article/details/103332901?utm_medium=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param
2. https://www.getit01.com/p20181212250511558/